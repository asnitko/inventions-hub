# Frontend

Install as you're used to. There's a more detailed manual inside `frontend` directory of the repo.

# Backend

In `frontend/src/environments/environments.ts` there're settings for backend API. An instance of backend is already deployed on a VPS, so you don't have to worry about it, just do `ng serve` and you're good to go. You may spin up your own instance of backend if you so like though. Instructions are inside the `backend/README.md` directory. If you do, don't forget to fix up `frontend/src/environments/environment.ts`: comment out lines containing `knards.com` and uncomment those containing `localhost:8000`.

# Theoretical questions from the task Word document

Like, was I really supposed to answer them? There were right answers written out just below the questions list :-/ Plus I've got Google... I hope this won't be the reason if I'm to fail this.
