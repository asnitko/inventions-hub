from django.contrib.auth import get_user_model
from rest_framework import serializers

from api.models import Invention


class InventionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Invention
        fields = '__all__'


class FullUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = '__all__'

    def create(self, validated_data):
        user = get_user_model().objects.create_user(**validated_data)
        return user


class ShortUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = ('username',)
