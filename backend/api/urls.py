from django.urls import path
from rest_framework import routers

from api.views import InventionsViewSet
from api.views import UsersViewSet

router = routers.DefaultRouter()
router.register('inventions', InventionsViewSet)
router.register('users', UsersViewSet)

urlpatterns = router.urls
