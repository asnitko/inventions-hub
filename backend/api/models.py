from datetime import datetime
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator


class Invention(models.Model):
    name = models.CharField(max_length=70, blank=False, null=False)
    author = models.CharField(max_length=30, blank=False, null=False)
    year = models.PositiveIntegerField(
        validators=[
            MinValueValidator(0),
            MaxValueValidator(datetime.now().year)],
        help_text="Type in year from 0 to {}".format(datetime.now().year),
        blank=False,
        null=False
    )
    longitude = models.DecimalField(
        validators=[
            MinValueValidator(-180),
            MaxValueValidator(180)
        ],
        help_text="Type in number from -180 to 180",
        blank=False,
        null=False,
        max_digits=9,
        decimal_places=6
    )
    latitude = models.DecimalField(
        validators=[
            MinValueValidator(-90),
            MaxValueValidator(90)
        ],
        help_text="Type in number from -90 to 90",
        blank=False,
        null=False,
        max_digits=8,
        decimal_places=6
    )
    comment = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        default=''
    )
    imageUrl = models.URLField(default='', blank=True, null=True)
