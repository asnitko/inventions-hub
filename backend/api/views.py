from django.contrib.auth import get_user_model
from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions

from api.models import Invention
from api.serializers import InventionSerializer
from api.serializers import FullUserSerializer, ShortUserSerializer


class InventionsViewSet(ModelViewSet):
    queryset = Invention.objects.all()
    serializer_class = InventionSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class UsersViewSet(ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = ShortUserSerializer
    permission_classes = (permissions.AllowAny,)

    def get_serializer_class(self):
        if self.action == 'list' or self.action == 'retrieve':
            return ShortUserSerializer
        return FullUserSerializer
