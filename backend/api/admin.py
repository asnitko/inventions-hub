from django.contrib import admin

from api.models import Invention


admin.site.register(Invention)
