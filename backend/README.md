# Backend

First off, install python and pip you don't have them installed.

# Virtual environment

Install pipenv:
`pip install --user pipenv`

Add `~/.local/bin` to your `PATH` env var (if you're on Linux; if not - google up how to do it on your OS):
`export PATH=$PATH":~/.local/bin"`

# Activate venv

Inside `backend` directory, run:
`pipenv install`

Then `pipenv shell` and `python manage.py runserver`. If no errors issued, you're good to go. If some did - google it. But really, just use the deployed instance.
