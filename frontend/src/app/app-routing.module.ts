import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InventionsComponent } from './inventions/inventions.component';
import { AddInventionComponent } from './inventions/add-invention/add-invention.component';
import { EditInventionsComponent } from './inventions/edit-inventions/edit-inventions.component';
import { SignUpComponent } from './signup/signup.component';
import { SignInComponent } from './signin/signin.component';

import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: '/inventions', pathMatch: 'full' },
  { path: 'inventions', component: InventionsComponent },
  { path: 'inventions/add', canActivate: [AuthGuard], component: AddInventionComponent },
  { path: 'inventions/edit', canActivate: [AuthGuard], component: EditInventionsComponent },
  { path: 'sign-up', component: SignUpComponent },
  { path: 'sign-in', component: SignInComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
