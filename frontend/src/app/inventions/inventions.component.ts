import { Component, OnInit } from '@angular/core';
import { Map } from 'mapbox-gl';

import { InventionsService } from '../services/inventions.service';
import { Invention } from '../shared/invention.interface';

@Component({
  selector: 'inventions',
  templateUrl: './inventions.component.html',
  styleUrls: ['./inventions.component.scss']
})
export class InventionsComponent implements OnInit {
  public map: Map;
  public markers: Invention[] = [];
  public centuries = [];
  public selectedCentury = 0;
  public emptyMarker: Invention = {name: '', author: '', year: 0, longitude: '', latitude: '', comment: '', imageUrl: ''};
  public hoverOnMarker: Invention = this.emptyMarker;

  constructor(private _inventionsService: InventionsService) {}

  ngOnInit() {
    this._inventionsService.getInventions().subscribe(
      (inventions: Invention[]) => {
        this.markers = inventions;
        for (const inv of inventions) {
          this.centuries.push(Math.ceil(+inv.year / 100));
        }
        this.centuries = this.centuries.filter((el, i, a) => i === a.indexOf(el)).sort();
      }
    );
  }

  public onMarkerHover(marker: Invention) {
    this.hoverOnMarker = marker;
  }

  public onMarkerLeave() {
    this.hoverOnMarker = this.emptyMarker;
  }

  public onFly(coordinates: {longitude: number, latitude: number}) {
    try {
      this.map.flyTo({center: [coordinates.longitude, coordinates.latitude]});
    } catch (err) {
      alert('Map is not fully loaded yet.');
    }
  }
}
