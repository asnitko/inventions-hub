import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { InventionsService } from '../../services/inventions.service';
import { Invention } from '../../shared/invention.interface';

@Component({
  selector: 'edit-inventions',
  templateUrl: './edit-inventions.component.html',
  styleUrls: ['./edit-inventions.component.scss']
})
export class EditInventionsComponent implements OnInit {
  public inventions: Invention[] = [];
  public emptyInvention: Invention = {id: 0, name: '', author: '', year: 0, longitude: '', latitude: '', comment: '', imageUrl: ''};
  public selectedInvention: Invention = this.emptyInvention;

  constructor(private _inventionsService: InventionsService) {}

  ngOnInit() {
    this._inventionsService.getInventions().subscribe(
      (inventions: Invention[]) => {
        this.inventions = inventions;
        this.selectedInvention = this.emptyInvention;
      }
    );
  }

  public onSubmit(form: NgForm) {
    if (form.valid) {
      this._inventionsService.editInvention({
        id: this.selectedInvention.id,
        name: form.controls.name.value,
        author: form.controls.author.value,
        year: form.controls.year.value,
        longitude: form.controls.long.value,
        latitude: form.controls.lat.value,
        comment: form.controls.comment.value,
        imageUrl: form.controls.imageUrl.value,
      })
      .subscribe(
        (response: Invention) => {
          this._inventionsService.getInventions().subscribe(
            (inventions: Invention[]) => {
              this.inventions = inventions;
              this.selectedInvention = this.emptyInvention;
              form.reset();
            }
          );
        }
      );
    }
  }

  public onDelete() {
    const sure = confirm('Are you sure?');
    if (sure) {
      this._inventionsService.deleteInvention(+this.selectedInvention.id)
        .subscribe(
          response => {
            this._inventionsService.getInventions().subscribe(
              (inventions: Invention[]) => {
                this.inventions = inventions;
                this.selectedInvention = this.emptyInvention;
              }
            );
          }
        );
    }
  }

}
