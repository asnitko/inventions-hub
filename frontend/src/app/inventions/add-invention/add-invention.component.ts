import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { InventionsService } from '../../services/inventions.service';
import { Invention } from '../../shared/invention.interface';

@Component({
  selector: 'add-invention',
  templateUrl: './add-invention.component.html',
  styleUrls: ['./add-invention.component.scss']
})
export class AddInventionComponent {

  constructor(
    private _inventionsService: InventionsService,
    private _router: Router
  ) {}

  public onSubmit(form: NgForm) {
    if (form.valid) {
      this._inventionsService.addInvention({
        name: form.controls.name.value,
        author: form.controls.author.value,
        year: form.controls.year.value,
        longitude: form.controls.long.value,
        latitude: form.controls.lat.value,
        comment: form.controls.comment.value,
        imageUrl: form.controls.imageUrl.value,
      })
      .subscribe(
        (response: Invention) => {
          this._router.navigate(['/']);
        }
      );
    }
  }

}
