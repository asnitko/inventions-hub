import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Invention } from '../../../shared/invention.interface';

@Component({
  selector: 'single-invention',
  templateUrl: './single-invention.component.html',
  styleUrls: ['./single-invention.component.scss']
})
export class SingleInventionComponent {
  @Input() invention: Invention;
  @Output() selectInvention = new EventEmitter<Invention>();
  @Output() hoverOnInvention = new EventEmitter<Invention>();
  @Output() hoverOffInvention = new EventEmitter<void>();

  constructor() { }

  public onClick() {
    this.selectInvention.emit(this.invention);
  }

  public onMouseOver() {
    this.hoverOnInvention.emit(this.invention);
  }

  public onMouseLeave() {
    this.hoverOffInvention.emit();
  }

}
