import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Invention } from '../../shared/invention.interface';

@Component({
  selector: 'list-inventions',
  templateUrl: './list-inventions.component.html',
  styleUrls: ['./list-inventions.component.scss']
})
export class ListInventionsComponent {
  @Input() inventions: Invention[];
  @Input() selectInventionAction: string;
  @Output() fly = new EventEmitter<{longitude: number, latitude: number}>();
  @Output() edit = new EventEmitter<Invention>();
  @Output() hover_on_marker = new EventEmitter<Invention>();
  @Output() hover_off_marker = new EventEmitter<void>();

  public onSelectInvention(invention: Invention, action: string) {
    if (action === 'fly') {
      this.fly.emit({
        longitude: +invention.longitude,
        latitude: +invention.latitude
      });
    } else if (action === 'edit') {
      this.edit.emit(invention);
    }
  }

  public onHover(object: any) {
    if (object) {
      this.hover_on_marker.emit(object);
    } else {
      this.hover_off_marker.emit();
    }
  }
}
