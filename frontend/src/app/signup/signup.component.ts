import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignUpComponent {

  constructor(
    private _authService: AuthService,
    private _router: Router
  ) {}

  onSubmit(form: NgForm) {
    if (form.valid) {
      if (form.controls.password.value !== form.controls.password2.value) {
        alert('Passwords don\'t coincide!');
      } else {
        this._authService.register(form.controls.username.value, form.controls.password.value)
          .subscribe(
            response => {
              this._router.navigate(['/sign-in']);
            },
            error => {
              alert('Error! Check browser console for details.');
              console.log(error);
            },
          );
      }
    }
  }

}
