import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SignUpComponent } from './signup/signup.component';
import { SignInComponent } from './signin/signin.component';
import { InventionsComponent } from './inventions/inventions.component';
import { AddInventionComponent } from './inventions/add-invention/add-invention.component';
import { EditInventionsComponent } from './inventions/edit-inventions/edit-inventions.component';
import { ListInventionsComponent } from './inventions/list-inventions/list-inventions.component';
import { SingleInventionComponent } from './inventions/list-inventions/single-invention/single-invention.component';

import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth.guard';
import { AuthInterceptor } from './services/auth.interceptor';
import { InventionsService } from './services/inventions.service';

import { DropdownDirective } from './shared/dropdown.directive';
import { FilterPipe } from './shared/filter.pipe';

import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SignUpComponent,
    SignInComponent,
    InventionsComponent,
    AddInventionComponent,
    EditInventionsComponent,
    ListInventionsComponent,
    SingleInventionComponent,
    DropdownDirective,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxMapboxGLModule.withConfig({
      accessToken: environment.mapbox.MAPBOX_API_TOKEN
    }),
    HttpClientModule,
    FormsModule
  ],
  providers: [
    AuthService,
    AuthGuard,
    InventionsService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
