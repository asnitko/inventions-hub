import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SignInComponent implements OnInit {
  @ViewChild('f') form: NgForm;

  constructor(
    private _authService: AuthService,
    private _router: Router
  ) { }

  ngOnInit() {
    // reset login status
    this._authService.logout();
    this.form.reset();
  }

  onSubmit(form: NgForm) {
    if (form.valid) {
      this._authService.login(form.controls.username.value, form.controls.password.value)
        .pipe(first())
        .subscribe(
          data => {
            this._router.navigate(['/']);
          },
          error => {
            alert(error['error']['non_field_errors']);
          }
        );
    }
  }

}
