import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { Invention } from '../shared/invention.interface';

@Injectable({
  providedIn: 'root'
})
export class InventionsService {

  constructor(private _http: HttpClient) { }

  public getInventions(): Observable<Invention[]> {
    return this._http.get<Invention[]>(environment.backend_api_url + '/inventions/')
      .pipe(map(data => {
        const ret = data;
        ret.sort((a, b) => a.latitude < b.latitude ? 1 : -1);
        return ret;
      }));
  }

  public addInvention(invention: Invention): Observable<Invention> {
    return this._http.post<Invention>(
      environment.backend_api_url + '/inventions/',
      invention
    );
  }

  public editInvention(invention: Invention): Observable<Invention> {
    return this._http.put<Invention>(
      environment.backend_api_url + '/inventions/' + invention.id + '/',
      invention
    );
  }

  public deleteInvention(id: number): Observable<any> {
    return this._http.delete(
      environment.backend_api_url + '/inventions/' + id + '/'
    );
  }

}
