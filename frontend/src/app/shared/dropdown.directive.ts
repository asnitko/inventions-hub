import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {
  @HostBinding('class.open') isOpen = false;

  @HostListener('click', ['$event']) toggleDropdown(event: any) {
    this.isOpen = !this.isOpen;
    event.preventDefault();
  }

  @HostListener('document:mousedown', ['$event'])
  onGlobalClick(event: any) {
    if (event.target.className !== 'dropdown-item') {
      this.isOpen = false;
    }
  }
}
