import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(value: any, filterString: number): any {
    if (value.length === 0 || filterString === 0) {
      return value;
    }
    const resultArray = [];
    for (const item of value) {
      if (Math.ceil(+item.year / 100) === filterString) {
        resultArray.push(item);
      }
    }
    return resultArray;
  }
}
