export interface Invention {
  id?: number;
  name: string;
  author: string;
  year: number;
  longitude: string;
  latitude: string;
  comment?: string;
  imageUrl: string;
}
