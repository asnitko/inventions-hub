// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  mapbox: {
    MAPBOX_API_TOKEN: 'pk.eyJ1Ijoic25pdGtvIiwiYSI6ImNqdzZzYzRnYzFjd20zenA5Z2N3cGJheTgifQ.w-UqW0EfKXC0_fbm6qahpg'
  },
  // Uncomment these props and comment out the following two to enable to use backend on localhost
  // backend_baseurl: 'http://localhost:8000',
  // backend_api_url: 'http://localhost:8000/api'
  backend_baseurl: 'http://knards.com',
  backend_api_url: 'http://knards.com/api'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
